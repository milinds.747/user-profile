package com.example.user.utils;

public class UserProfileConstants {

	private UserProfileConstants() { }
	
	public static final String DEFAULT_ORDERING = "asc";
	public static final String DEFAULT_PAGE_SIZE = "20";
	public static final String DEFAULT_PAGE_NUMBER = "0";
	public static final String ORDER_KEY = "order";
	public static final String SORT_BY_KEY = "sortBy";
	public static final String PAGE_SIZE_KEY = "pageSize";
	public static final String PAGE_NUMBER_KEY = "pageNumber";
	public static final String USER_ID_KEY = "userId";
	public static final String USER_ID_PATH_VARIABLE = "{userId}";

}
