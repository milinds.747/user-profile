package com.example.user.utils.enums;

public enum EmployeeType {
	INTERN, CONTRACTOR, FULL_TIME;
}
