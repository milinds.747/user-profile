package com.example.user.utils.exceptions;

import java.util.Set;

public class UserProfileCreationException extends UserProfileBaseException {

	private static final long serialVersionUID = 1L;
	public UserProfileCreationException(Set<String> collect) {
		super(collect);
	}
}
