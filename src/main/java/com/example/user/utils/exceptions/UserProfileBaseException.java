package com.example.user.utils.exceptions;

import java.util.Collections;
import java.util.Set;

public class UserProfileBaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final Set<String> errors;
	public UserProfileBaseException(Set<String> collect) {
		super(collect.toString());
		errors = Collections.unmodifiableSet(collect);
	}

	public Set<String> getErrors() {
		return errors;
	}
}
