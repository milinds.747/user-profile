package com.example.user.utils.exceptions;

import java.util.Set;

public class UserProfileUpdateException extends UserProfileBaseException {

	private static final long serialVersionUID = 1L;
	public UserProfileUpdateException(Set<String> collect) {
		super(collect);
	}

}
