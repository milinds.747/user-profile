package com.example.user.utils.validations;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.user.dto.request.UserRequestDto;
import com.example.user.utils.exceptions.UserProfileCreationException;
import com.example.user.utils.exceptions.UserProfileUpdateException;

@Component
public class UserProfileValidator {

	private static final Logger log = LoggerFactory.getLogger(UserProfileValidator.class);

	@Autowired
	private Validator validator;

	public void validateUserForCreateOrUpdate(UserRequestDto companyUser) {
		log.info("Starting validations for company user: [{}]", companyUser);
		Set<ConstraintViolation<UserRequestDto>> violations = validator.validate(companyUser);
		if (violations.isEmpty()) {
			log.info("Validation succeeded for company user bean: [{}]", companyUser);
			return;
		}
		log.warn("validation failed for company user bean");
		if (companyUser.getUserId().isEmpty()) {
			throw new UserProfileCreationException(
					violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet()));
		}
		throw new UserProfileUpdateException(
				violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet()));
	}
}
