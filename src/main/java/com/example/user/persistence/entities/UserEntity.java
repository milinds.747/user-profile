package com.example.user.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.example.user.dto.request.UserRequestDto;

@Entity
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	private String firstName;
	private String lastName;
	private String gender;
	private String category;
	private String businessUnit;
	private String email;
	private String mobileNumber;

	public UserEntity() { }

	public UserEntity(UserRequestDto companyUser) {
		this.firstName = companyUser.getFirstName();
		this.lastName = companyUser.getLastName();
		this.email = companyUser.getEmail();
		this.gender = companyUser.getGender().toString();
		this.category = companyUser.getCategory().toString();
		this.businessUnit = companyUser.getBusinessUnit();
		this.mobileNumber = companyUser.getMobileNumber();
		this.userId = companyUser.getUserId().orElse(null);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
