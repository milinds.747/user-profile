package com.example.user.persistence.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.user.persistence.entities.UserEntity;

public interface CompanyUserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	List<UserEntity> findByEmail(String email);
}
