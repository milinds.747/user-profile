package com.example.user.service;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.user.dto.request.UserRequestDto;
import com.example.user.dto.response.PageResponse;
import com.example.user.persistence.entities.UserEntity;
import com.example.user.persistence.repo.CompanyUserRepository;
import com.example.user.utils.exceptions.UserProfileCreationException;
import com.example.user.utils.exceptions.UserProfileUpdateException;
import com.example.user.utils.validations.UserProfileValidator;

@Service
public class UserProfileService {

	private static final Logger log = LoggerFactory.getLogger(UserProfileService.class);
	
	@Autowired
	private UserProfileValidator userProfileValidator;
	
	@Autowired
	private CompanyUserRepository companyUserRepository;
	
	public Long createUser(UserRequestDto userRequestDto) {
		userProfileValidator.validateUserForCreateOrUpdate(userRequestDto);
		if (!companyUserRepository.findByEmail(userRequestDto.getEmail()).isEmpty()) {
			log.warn("email: [{}] is already registered, cannot proceed with user creation.", userRequestDto.getEmail());
			throw new UserProfileCreationException(Set.of(String.format("Unable to create user with email: [%s]", userRequestDto.getEmail())));
		}
		UserEntity persistedUser = companyUserRepository.save(new UserEntity(userRequestDto));
		log.info("user with email: [{}] created successfully, assigned user id: [{}]", userRequestDto.getEmail(), persistedUser.getUserId());
		return persistedUser.getUserId();
	}

	public Long updateUser(final UserRequestDto userRequestDto) {
		userProfileValidator.validateUserForCreateOrUpdate(userRequestDto);
		if (userRequestDto.getUserId().isEmpty()) {
			String errorMessage = "Unable to update user, if not provided";
			throw new UserProfileUpdateException(Set.of(errorMessage));
		}
		Long userId = userRequestDto.getUserId().get();
		Optional<UserEntity> existingUser = companyUserRepository.findById(userId);
		if (existingUser.isEmpty()) {
			String errorMessage = String.format("Unable to update user with id: [%s]", userId);
			throw new UserProfileUpdateException(Set.of(errorMessage));
		}
		UserEntity persistedUser = companyUserRepository.save(new UserEntity(userRequestDto));
		log.info("user: [{}] updated successfully", persistedUser.getUserId());
		return persistedUser.getUserId();
	}
	
	public UserEntity find(Long userId) {
		return companyUserRepository.findById(userId).orElse(null);
	}
	
	public PageResponse findPage(PageRequest pageRequest) {
		Page<UserEntity> data = companyUserRepository.findAll(pageRequest);
		return new PageResponse(data.getNumber(), data.getSize(), data.getTotalElements(), data.getTotalPages(), data.getContent());
	}

	public void remove(Long userId) {
		companyUserRepository.deleteById(userId);
	}
}
