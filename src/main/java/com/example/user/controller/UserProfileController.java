package com.example.user.controller;

import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.user.dto.request.UserRequestDto;
import com.example.user.dto.response.PageResponse;
import com.example.user.dto.response.SuccessResponse;
import com.example.user.persistence.entities.UserEntity;
import com.example.user.service.UserProfileService;
import com.example.user.utils.UserProfileConstants;

@RestController
@RequestMapping("user")
public class UserProfileController {
	
	@Autowired
	private UserProfileService userProfileService;
	
	@PostMapping
	public ResponseEntity<SuccessResponse> createUser(@RequestBody UserRequestDto userRequestDto) {
		Long userId = userProfileService.createUser(userRequestDto);
		return ResponseEntity.ok(new SuccessResponse("User create operation successful!", Map.of(UserProfileConstants.USER_ID_KEY, userId)));
	}

	@PutMapping(UserProfileConstants.USER_ID_PATH_VARIABLE)
	public ResponseEntity<SuccessResponse> updateOrCreate(@RequestBody UserRequestDto userRequestDto, @PathVariable Long userId) {
		userRequestDto.setUserId(userId);
		userProfileService.updateUser(userRequestDto);
		return ResponseEntity.ok(new SuccessResponse("User update operation successful!", Map.of(UserProfileConstants.USER_ID_KEY, userId)));
	}
	
	@GetMapping
	public ResponseEntity<PageResponse> find(
				@RequestParam(value = UserProfileConstants.PAGE_NUMBER_KEY, defaultValue = UserProfileConstants.DEFAULT_PAGE_NUMBER) Integer pageNumber,
				@RequestParam(value = UserProfileConstants.PAGE_SIZE_KEY, defaultValue = UserProfileConstants.DEFAULT_PAGE_SIZE) Integer pageSize,
				@RequestParam(value = UserProfileConstants.SORT_BY_KEY, defaultValue = UserProfileConstants.USER_ID_KEY) String sortBy,
				@RequestParam(value = UserProfileConstants.ORDER_KEY, defaultValue = UserProfileConstants.DEFAULT_ORDERING) String direction) {
		
		PageRequest page = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.fromString(direction), sortBy));
		return ResponseEntity.ok(userProfileService.findPage(page));
	}
	
	@GetMapping(UserProfileConstants.USER_ID_PATH_VARIABLE)
	public ResponseEntity<SuccessResponse> find(@PathVariable Long userId) {
		UserEntity user = userProfileService.find(userId);
		return Objects.isNull(user) ?
			new ResponseEntity<>(new SuccessResponse(String.format("No user found with id: [%s]", userId), null), HttpStatus.NOT_FOUND)
			: ResponseEntity.ok(new SuccessResponse(String.format("No user found with id: [%s]", userId), null));
	}
	
	@DeleteMapping(UserProfileConstants.USER_ID_PATH_VARIABLE)
	public ResponseEntity<SuccessResponse> removeUser(@PathVariable Long userId) {
		userProfileService.remove(userId);
		return ResponseEntity.ok(new SuccessResponse("User remove operation successfully!", Map.of(UserProfileConstants.USER_ID_KEY, userId)));
	}
}
