package com.example.user.controller.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.user.dto.response.ErrorResponse;
import com.example.user.utils.exceptions.UserProfileBaseException;
import com.example.user.utils.exceptions.UserProfileCreationException;
import com.example.user.utils.exceptions.UserProfileUpdateException;

@RestControllerAdvice
public class UserProfileControllerAdvice {

	private static final Logger log = LoggerFactory.getLogger(UserProfileControllerAdvice.class);
	public static final String OPERATION_FAILED_MESSAGE = "Operation Failed";

	@ExceptionHandler(UserProfileCreationException.class)
	public ResponseEntity<ErrorResponse> handleUserProfileCreationException(UserProfileCreationException e) {
		log.error("Unable to create new user: \n", e);
		ErrorResponse errorResponse = new ErrorResponse(e.getErrors());
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserProfileUpdateException.class)
	public ResponseEntity<ErrorResponse> handleUserProfileUpdateException(UserProfileUpdateException e) {
		log.error("Unable to update user: \n", e);
		ErrorResponse errorResponse = new ErrorResponse(e.getErrors());
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserProfileBaseException.class)
	public ResponseEntity<ErrorResponse> handleUserProfileBaseException(UserProfileBaseException e) {
		log.error("Unable to perform given operation: \n", e);
		ErrorResponse errorResponse = new ErrorResponse(e.getErrors());
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
}
