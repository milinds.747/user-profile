package com.example.user.dto.request;

import java.util.Objects;
import java.util.Optional;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.example.user.utils.enums.EmployeeType;
import com.example.user.utils.enums.Gender;

public class UserRequestDto {

	@Pattern(regexp = "^[a-zA-Z]+$", message = "Please enter a valid first name")
	@NotBlank(message = "First name is mandatory for user create/ update operation")
	private String firstName;
	
	@Pattern(regexp = "^[a-zA-Z]+$", message = "Please enter a valid last name")
	@NotBlank(message = "Last name is mandatory for user create/ update operation")
	private String lastName;
	
	@NotNull(message = "Gender is mandatory for user create/ update operation")
	private Gender gender;
	
	@NotNull(message = "Employee category is mandatory for user create/ update operation")
	private EmployeeType category;
	
	@NotBlank(message = "Business unit is mandatory for user create/ update operation")
	private String businessUnit;
	
	@Email(message = "Please enter a valid email")
	@NotBlank(message = "Employee email is mandatory for user create/ update operation")
	private String email;
	
	@NotBlank(message = "Employee mobile number is mandatory for user create/ update operation")
	@Pattern(regexp = "^[789][0-9]{9}$", message = "Please enter a valid mobile number")
	private String mobileNumber;
	
	private Optional<Long> userId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(String gender) {
		try {
			this.gender = Gender.valueOf(gender.toUpperCase());
		} catch (Exception ignore) {
			this.gender = null;
		}
	}

	public EmployeeType getCategory() {
		return category;
	}

	public void setCategory(String category) {
		try {
			this.category = EmployeeType.valueOf(category.toUpperCase());
		} catch (Exception ignore) {
			this.category = null;
		}
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Optional<Long> getUserId() {
		return Objects.isNull(userId) ? Optional.empty() : userId;
	}

	public void setUserId(Long userId) {
		this.userId = Optional.ofNullable(userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(businessUnit, category, email);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRequestDto other = (UserRequestDto) obj;
		return Objects.equals(businessUnit, other.businessUnit) && category == other.category
				&& Objects.equals(email, other.email);
	}

	@Override
	public String toString() {
		return "CompanyUser [firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender + ", category="
				+ category + ", businessUnit=" + businessUnit + ", emailId=" + email + ", mobileNumber="
				+ mobileNumber + "]";
	}

}
