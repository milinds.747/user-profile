package com.example.user.dto.response;

import java.util.Objects;

public record SuccessResponse(String message, Object details) {
	public static final boolean success = true;
	public SuccessResponse(String message, Object details) {
		if (Objects.isNull(message) || message.isEmpty()) {
			this.message = "Operation Successful!";
		} else {
			this.message = message;
		}
		this.details = details;
	}
}
