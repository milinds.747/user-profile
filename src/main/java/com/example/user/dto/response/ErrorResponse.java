package com.example.user.dto.response;

import java.util.Collection;

public record ErrorResponse(Collection<String> errors) {
	public static final boolean success = false;
	public static final String message = "Operation failed!";
}
