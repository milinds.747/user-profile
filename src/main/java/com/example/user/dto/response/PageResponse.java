package com.example.user.dto.response;

import java.util.List;

@SuppressWarnings("rawtypes")
public record PageResponse(int pageNumber, int pageSize, long totalRecords, int totalPages, List users) {
	public static final boolean success = true;
	public static final String message = "Operation successful!";
}
