FROM openjdk:17.0.2-oraclelinux8
COPY ./user-profile-0.0.1.jar ./user-profile-0.0.1.jar
CMD java -jar user-profile-0.0.1.jar
EXPOSE 8080